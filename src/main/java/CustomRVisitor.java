public class CustomRVisitor extends RBaseVisitor{

    @Override
    public Object visitPrintExpr(RParser.PrintExprContext ctx) {
        Integer value = ctx.expr().value;
        System.out.println(value);
        return 0;
    }

    @Override
    public Object visitLoop(RParser.LoopContext ctx) {
        return super.visitLoop(ctx);
    }

    @Override
    public Object visitRepeat(RParser.RepeatContext ctx) {
        while (true) {
            for (RParser.StatContext stat : ctx.stat()) {
                this.visit(stat);
            }
        }
    }
}
