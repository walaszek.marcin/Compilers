import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

public class Compilers {
    public static void main(String[] args) throws Exception {
        InputStream is = getInputStream();
        ANTLRInputStream input = new ANTLRInputStream(is);
        RLexer lexer = new RLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        RParser parser = new RParser(tokens);
        new CustomRVisitor().visit(parser.prog());
    }

    private static InputStream getInputStream() throws FileNotFoundException {
        String inputFile = "testRCode.txt";
        return new FileInputStream(inputFile);
    }
}