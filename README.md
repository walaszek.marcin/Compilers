# Interpreter języka R

## Stack technologiczny

* Java 8
* ANTLR
* Maven

Generator ANTLR został wybrany ze względu na możliwość stosowania języka Java, który jest preferowany przez autora projektu. Ponadto, w internecie można znaleźć dość duż zbiór przykładowych gramatyk.
Dodatkowym atutem jest integracja z Mavenem oraz Gradle co ułatwia automatyzację pracy z parserem. 

## Uruchomienie programu
* uruchom goal Mavena nazwany antlr4 - zostanie wygenerowany kod na podstawie gramatyki zawartej w pliku R.g4.
* uruchom metodę main() z klasy Compilers.java.
* Kod w R który zostanie wykonany jest zawarty w pliku testRCode.txt.

## Problemy
Podczas wykonywania projektu największym problemem była mała ilość dokumentacji w zakresie tworzenia interpreterów dla paresa ANTLR. 
Z tego powodu większość czasu pochłonęło zdobywanie informacji w jaki sposób osiągnąć oczekiwany rezultat.
Kolejnym problemem była nieznajomośc języka R, który miał być przedmiotem interpretacji. W rezultacie konieczne było poświęcenie czasu na przyswojenie podstawowej składni języka. 

Powyższe problemy spowodowały, że zakres interpretacji obejmuje bardzo niewielki podzbiór jężyka R.  

## Linkografia
* https://tomassetti.me/antlr-mega-tutorial/
* http://meri-stuff.blogspot.com/2011/08/antlr-tutorial-hello-word.html
* https://theantlrguy.atlassian.net/wiki/spaces/ANTLR3/pages/2687210/Quick+Starter+on+Parser+Grammars+-+No+Past+Experience+Required
* https://stackoverflow.com/questions/23098415/visitor-listener-code-for-a-while-loop-in-antlr-4
* http://rextester.com/l/r_online_compiler

